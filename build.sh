#!/usr/bin/env bash

set -e


# The fsl/basisfield repository contains
# some tests/benchmark data stored using
# git lfs. We don't want the benchmark
# data to be part of the conda package,
# so we explicitly delete it here.
rm -rf tests

export FSLDIR=$PREFIX
export FSLDEVDIR=$PREFIX

. $FSLDIR/etc/fslconf/fsl-devel.sh

make insertcopyright

mkdir -p $PREFIX/src/
cp -r $(pwd) $PREFIX/src/$PKG_NAME

make
make install
